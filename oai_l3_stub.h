/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file oai_l3_stub.c
 * \brief An example of how to exchange messages between OAI and Velcore App via a control socket
 * \author Tien-Thinh NGUYEN
 * \date 20/11/2017
 * \version 0.1
 * \email:tien-thinh.nguyen@eurecom.fr
 * \company Eurecom
 */

#ifndef OAI_L3_STUB_H
#define OAI_L3_STUB_H

#define DEBUG_CTRL_SOCKET
#define BUFSIZE 1024
#define CONTROL_SOCKET_PORT_NO 8888

//netlink
//#define DEBUG_PDCP
#define UE_IP_PDCP_NETLINK_ID 31
#define PDCP_PID 1
#define NETLINK_HEADER_SIZE 16
#define SL_DEFAULT_RAB_ID     1
#define SLRB_ID              11

#define MAX_PAYLOAD 1024 /* maximum payload size*/

#define UE_STATE_NOTIFICATION_INTERVAL      50

#define IPV4_ADDR    "%u.%u.%u.%u"
#define IPV4_ADDR_FORMAT(aDDRESS)                 \
      (uint8_t)((aDDRESS)  & 0x000000ff),         \
      (uint8_t)(((aDDRESS) & 0x0000ff00) >> 8 ),  \
      (uint8_t)(((aDDRESS) & 0x00ff0000) >> 16),  \
      (uint8_t)(((aDDRESS) & 0xff000000) >> 24)


//-----------------------------------------------------
// header for Control socket

//Primitives
#define SESSION_INIT_REQ                    1
#define UE_STATUS_INFO                      2
#define GROUP_COMMUNICATION_ESTABLISH_REQ   3
#define GROUP_COMMUNICATION_ESTABLISH_RSP   4
#define DIRECT_COMMUNICATION_ESTABLISH_REQ  5
#define DIRECT_COMMUNICATION_ESTABLISH_RSP  6
#define GROUP_COMMUNICATION_RELEASE_REQ     7
#define GROUP_COMMUNICATION_RELEASE_RSP     8

typedef enum {
   UE_STATE_OFF_NETWORK,
   UE_STATE_ON_NETWORK
} UE_STATE_t;

typedef enum {
   GROUP_COMMUNICATION_RELEASE_OK = 0,
   GROUP_COMMUNICATION_RELEASE_FAILURE
} Group_Communication_Status_t;

struct GroupCommunicationEstablishReq {
   uint8_t type; //0 - rx, 1 - tx
   uint32_t sourceL2Id;
   uint32_t groupL2Id;
   uint32_t groupIpAddress;
   uint8_t pppp;
};

struct DirectCommunicationEstablishReq {
   uint32_t sourceL2Id;
   uint32_t destinationL2Id;
};

struct sidelink_ctrl_element {
   unsigned short type;
   union {
      struct GroupCommunicationEstablishReq group_comm_establish_req;
      struct DirectCommunicationEstablishReq direct_comm_estblish_req;
      Group_Communication_Status_t group_comm_release_rsp;
      //struct DirectCommunicationReleaseReq  direct_comm_release_req;
      UE_STATE_t ue_state;
      int slrb_id;

   } sidelinkPrimitive;
};

//global variables
extern struct sockaddr_in clientaddr;
extern int slrb_id;
extern pthread_mutex_t slrb_mutex;


void error(char *msg);
//the thread function
void *send_UE_status_notification(void *);

//----------------------------------------------------
//Header for netlink socket
typedef enum  ip_traffic_type_e {
   TRAFFIC_IPVX_TYPE_UNKNOWN    =  0,
   TRAFFIC_IPV6_TYPE_UNICAST    =  1,
   TRAFFIC_IPV6_TYPE_MULTICAST  =  2,
   TRAFFIC_IPV6_TYPE_UNKNOWN    =  3,
   TRAFFIC_IPV4_TYPE_UNICAST    =  5,
   TRAFFIC_IPV4_TYPE_MULTICAST  =  6,
   TRAFFIC_IPV4_TYPE_BROADCAST  =  7,
   TRAFFIC_IPV4_TYPE_UNKNOWN    =  8
} ip_traffic_type_t;

typedef int32_t               sdu_size_t;
typedef uint16_t              rb_id_t;

typedef struct pdcp_data_req_header_t {
   rb_id_t             rb_id;
   sdu_size_t           data_size;
   signed int        inst;
   ip_traffic_type_t   traffic_type;
} pdcp_data_req_header_t;

typedef struct pdcp_data_ind_header_t {
   rb_id_t             rb_id;
   sdu_size_t           data_size;
   ip_traffic_type_t   traffic_type;
   int       inst;

} pdcp_data_ind_header_t;

#define UE_IP_PDCPH_SIZE   (int)sizeof(struct pdcp_data_req_header_t)

//thread function to listen to netlink socket
void *listen_netlink_socket_pdcp(void *);



#endif
