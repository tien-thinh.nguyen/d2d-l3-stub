/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file v2x_app.c
 * \brief An example of how to exchange messages between OAI and Prose App via a control socket
 * \author Tien-Thinh NGUYEN
 * \date 20/11/2017
 * \version 0.1
 * \email:tien-thinh.nguyen@eurecom.fr
 * \company Eurecom
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include<pthread.h>
#include <arpa/inet.h>
#include <stdbool.h>


#define IPV4_ADDR    "%u.%u.%u.%u"
#define IPV4_ADDR_FORMAT(aDDRESS)                 \
      (uint8_t)((aDDRESS)  & 0x000000ff),         \
      (uint8_t)(((aDDRESS) & 0x0000ff00) >> 8 ),  \
      (uint8_t)(((aDDRESS) & 0x00ff0000) >> 16),  \
      (uint8_t)(((aDDRESS) & 0xff000000) >> 24)

#define DEBUG
#define BUFSIZE 1024
#define CONTROL_SOCKET_PORT_NO 8888
#define PDCP_SOCKET_PORT_NO 9999
#define MAC_SOCKET_PORT_NO 9998
#define SLRB_TO_BE_RELEASED 3
#define PC5S_SLRB  10

//Primitives
#define SESSION_INIT_REQ                    1
#define UE_STATUS_INFO                      2
#define GROUP_COMMUNICATION_ESTABLISH_REQ   3
#define GROUP_COMMUNICATION_ESTABLISH_RSP   4
#define DIRECT_COMMUNICATION_ESTABLISH_REQ  5
#define DIRECT_COMMUNICATION_ESTABLISH_RSP  6
#define GROUP_COMMUNICATION_RELEASE_REQ     7
#define GROUP_COMMUNICATION_RELEASE_RSP     8
#define PC5S_ESTABLISH_REQ                  9
#define PC5S_ESTABLISH_RSP                  10
#define PC5_DISCOVERY_MESSAGE               11


#define PC5_DISCOVERY_PAYLOAD_SIZE          29
#define PC5_SIGNALLING_PAYLOAD_SIZE         5 //should be updated with a correct value

typedef int32_t               sdu_size_t;
typedef uint16_t              rb_id_t;

typedef enum {
   UE_STATE_OFF_NETWORK,
   UE_STATE_ON_NETWORK
} UE_STATE_t;

typedef enum {
   GROUP_COMMUNICATION_RELEASE_OK = 0,
   GROUP_COMMUNICATION_RELEASE_FAILURE
} Group_Communication_Status_t;

struct GroupCommunicationEstablishReq {
   uint8_t type; //0 - rx, 1 - tx
   uint32_t sourceL2Id;
   uint32_t groupL2Id;
   uint32_t groupIpAddress;
   uint8_t pppp;
};

struct DirectCommunicationEstablishReq {
   uint32_t sourceL2Id;
   uint32_t destinationL2Id;
   uint8_t pppp;
};

struct PC5SEstablishReq{
   uint8_t  type; //0-rx, 1-tx
   uint32_t sourceL2Id;
   uint32_t destinationL2Id;
};


struct PC5SEstablishRsp{
   uint32_t slrbid_lcid28;
   uint32_t slrbid_lcid29;
   uint32_t slrbid_lcid30;
};


//PC5_DISCOVERY MESSAGE
typedef struct  {
   unsigned char bytes[PC5_DISCOVERY_PAYLOAD_SIZE];
   uint32_t measuredPower;
}  __attribute__((__packed__)) PC5DiscoveryMessage ;



struct sidelink_ctrl_element {
   unsigned short type;
   union {
      struct GroupCommunicationEstablishReq group_comm_establish_req;
      struct DirectCommunicationEstablishReq direct_comm_estblish_req;
      Group_Communication_Status_t group_comm_release_rsp;
      //struct DirectCommunicationReleaseReq  direct_comm_release_req;
      UE_STATE_t ue_state;
      int slrb_id;
      struct PC5SEstablishReq pc5s_establish_req;
      struct PC5SEstablishRsp pc5s_establish_rsp;
      PC5DiscoveryMessage pc5_discovery_message;
   } sidelinkPrimitive;
};


typedef enum  ip_traffic_type_e {
   TRAFFIC_IPVX_TYPE_UNKNOWN    =  0,
   TRAFFIC_IPV6_TYPE_UNICAST    =  1,
   TRAFFIC_IPV6_TYPE_MULTICAST  =  2,
   TRAFFIC_IPV6_TYPE_UNKNOWN    =  3,
   TRAFFIC_IPV4_TYPE_UNICAST    =  5,
   TRAFFIC_IPV4_TYPE_MULTICAST  =  6,
   TRAFFIC_IPV4_TYPE_BROADCAST  =  7,
   TRAFFIC_IPV4_TYPE_UNKNOWN    =  8,
   TRAFFIC_PC5S_SIGNALLING      = 9,
   TRAFFIC_PC5S_SESSION_INIT    = 10
} ip_traffic_type_t;


typedef struct  {
   rb_id_t             rb_id;
   sdu_size_t          data_size;
   signed int          inst;
   ip_traffic_type_t   traffic_type;
   uint32_t sourceL2Id;
   uint32_t destinationL2Id;
} __attribute__((__packed__)) pdcp_data_header_t;

//new PC5S-message
typedef struct  {
   unsigned char bytes[PC5_SIGNALLING_PAYLOAD_SIZE];
}  __attribute__((__packed__)) PC5SignallingMessage ;

//example of PC5-S messages
typedef struct {
   pdcp_data_header_t pdcp_data_header;
   union {
      uint8_t status;
      PC5SignallingMessage pc5_signalling_message;
   } pc5sPrimitive;
} __attribute__((__packed__)) sidelink_pc5s_element;


//global variables
//Control socket
extern int ctrl_sockfd;
extern struct sockaddr_in ctrl_sin;
//PDCP socket
extern int pdcp_sockfd;
extern struct sockaddr_in pdcp_sin;
//MAC socket
extern int mac_sockfd;
extern struct sockaddr_in mac_sin;

//global variables
int ctrl_sockfd;
struct sockaddr_in ctrl_sin;
int pdcp_sockfd;
struct sockaddr_in pdcp_sin;
int mac_sockfd;
struct sockaddr_in mac_sin;

/**
\brief Establish a connection with OAI Control socket
@return status of the UE*/
int session_init();

/**
\brief Send a request to establish a SLRB for a group communication
@param type 0 - for RX, 1 for TX
@param source_id Layer2 ID of the source
@param group_id Group layer2 ID
@param pppp Represent packet-per-priority
@return SLRB ID Established for this communication*/
//int setup_PC5u_group_request(uint32_t source_id, uint32_t group_id, uint8_t pppp);
int setup_PC5u_group_request(uint8_t type, uint32_t source_id, uint32_t group_id, uint32_t group_ip_address, uint8_t pppp);

/**
\brief Send a request to release a SLRB for a group communication
@param SLRB ID Used for this communication
@returns 0 on success, -1 on failure*/
int release_PC5u_group_request(uint8_t slrb_id);

/**
\brief Establish a connection with OAI Control socket
@return status of the UE*/
int pdcp_session_init();

int  send_pc5s_message(sidelink_pc5s_element *sl_pc5s_msg );

// a thread function to receive UE status notification
void *rcv_UE_status_notification(void *);

// a thread function to receive UE status notification
void *generate_traffic();
void error(char *msg);

//for PC5-S
/**
\brief Send a request to establish a SLRB for a group communication
@param source_id Layer2 ID of the source
@param destination_id Destination layer2 ID
@return SLRB ID Established for this PC5S*/
//int setup_PC5u_group_request(uint32_t source_id, uint32_t group_id, uint8_t pppp);
int setup_PC5s_request(uint8_t type, uint32_t source_id, uint32_t destination_id);

int  send_PC5D_message(PC5DiscoveryMessage *pc5_discovery_message);

//--------------------------------------------------------
int main(int argc, char *argv[]) {

   int serverlen;
   struct hostent *server;
   char *hostname;
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   int status;
   int slrb_id;
   int n;
   bool sender_mode = false;
   bool receiver_mode = false;
   bool onetomany_scenario = false;
   bool discovery_mode = false;
   int opt;
   uint32_t receiver_id;

   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   sidelink_pc5s_element *sl_pc5s_msg_rcv = NULL;

   sidelink_pc5s_element *sl_pc5s_msg_send = NULL;
   PC5SignallingMessage *pc5_signalling_message = NULL;
   pdcp_data_header_t *pdcp_data_header = NULL;
   PC5DiscoveryMessage *pc5_discovery_message = NULL;

   while ((opt = getopt(argc, argv, "srd")) != -1) {
      switch (opt) {
      case 's':
         sender_mode = true;
         break;
      case 'r':
         receiver_mode = true;
         break;
      case 'd':
         discovery_mode = true;
         break;
      default:
         onetomany_scenario= true;
         break;
      }
   }

   if (sender_mode) printf ("One-to-one Scenario, Sender\n");
   else if (receiver_mode) printf ("One-to-one Scenario, Receiver\n");
   else if (discovery_mode) printf ("Discovery Scenario\n");
   else {
      onetomany_scenario= true;
      printf ("One-to-many Scenario\n");
   }
   printf("------------------------------------------------\n");

   hostname = "localhost";
   // create the socket
   ctrl_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (ctrl_sockfd < 0)
      error("ERROR: Failed to create control socket");

   server = gethostbyname(hostname);
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host as %s\n", hostname);
      exit(0);
   }
   // build the server's address
   memset(&ctrl_sin, 0, sizeof(struct sockaddr_in));
   ctrl_sin.sin_family = AF_INET;
   bcopy((char *)server->h_addr,
         (char *)&ctrl_sin.sin_addr.s_addr, server->h_length);
   ctrl_sin.sin_port = htons(CONTROL_SOCKET_PORT_NO);


   //create MAC socket
   mac_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (mac_sockfd < 0)
      error("ERROR: Failed to create MAC socket");
   memset(&mac_sin, 0, sizeof(struct sockaddr_in));
   mac_sin.sin_family = AF_INET;
   bcopy((char *)server->h_addr,
         (char *)&mac_sin.sin_addr.s_addr, server->h_length);
   mac_sin.sin_port = htons(MAC_SOCKET_PORT_NO);

   //create PDCP socket
   pdcp_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (pdcp_sockfd < 0)
      error("ERROR: Failed to create PDCP socket");
   memset(&pdcp_sin, 0, sizeof(struct sockaddr_in));
   pdcp_sin.sin_family = AF_INET;
   bcopy((char *)server->h_addr,
         (char *)&pdcp_sin.sin_addr.s_addr, server->h_length);
   pdcp_sin.sin_port = htons(PDCP_SOCKET_PORT_NO);


   //Init a session with OAI (control socket)
   status = session_init ();
   if (status == UE_STATE_ON_NETWORK ) {
      printf("[UEStatusInformation] UE is on coverage\n");
   } else if (status == UE_STATE_OFF_NETWORK) {
      printf("[UEStatusInformation] UE is out of coverage\n");
   }

   //send PC5_discovery_announcement message
   if (discovery_mode){
      sleep(10);
      //send PC5_DISCOVERY via Ctrl socket
      pc5_discovery_message = calloc(1, sizeof(PC5DiscoveryMessage));
      send_PC5D_message(pc5_discovery_message);

      //wait for discovery from other UEs
      printf("Waiting for incoming PC5_discovery_message from other UEs.... \n");
      memset(receive_buf, 0, BUFSIZE);
      serverlen = sizeof(ctrl_sin);
      while (1) {
         n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
         if (n < 0)
            error("ERROR: Failed to receive from OAI");

         sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
         if (sl_ctrl_msg->type == PC5_DISCOVERY_MESSAGE) {
            printf("Received PC5DiscoveryMessage on socket from OAI\n");
            //store UE Id which then can be used as the destination address of a PC5-S message
            // receiver_id = sl_ctrl_msg->sidelinkPrimitive.pc5_discovery_message.proSeUEId;
         }
      }
   }

   //One-to-one PC5-S
   if (sender_mode | receiver_mode) { //One-to-one
      //test PC5-S
      status = pdcp_session_init();

      //For sender
      if (sender_mode) {
         uint32_t source_id = 0x01; //hardcoded
         uint32_t destination_id = 0x02;  //hardcoded
         uint8_t type = 1;//tx
         //for the moment, we use only one SLRB_ID for PC5S signaling (instead of using 28,29,30)
         status = setup_PC5s_request(type, source_id, destination_id);
         if (status > -1 ){
            printf("[PC5S] SRLBs have been established for PC5S\n");
         } else{
            printf("[PC5S] Failed to establish SLRBs for PC5S\n");
         }

         //send a message over PC5-S
         printf("------------------------------------------------\n");
         //fill PDCP header
         pdcp_data_header = calloc(1, sizeof(pdcp_data_header_t));
         pdcp_data_header->traffic_type = TRAFFIC_PC5S_SIGNALLING; //DirectCommunicationRequest
         pdcp_data_header->rb_id = PC5S_SLRB; //for the moment, we use only one SLRB for all PC5S messages
         pdcp_data_header->data_size = sizeof(sidelink_pc5s_element);
         pdcp_data_header->inst = 0;
         pdcp_data_header->sourceL2Id = source_id;
         pdcp_data_header->destinationL2Id = destination_id;

         //fill the message
         pc5_signalling_message = calloc(1, sizeof(PC5SignallingMessage));

         sl_pc5s_msg_send = calloc(1, sizeof(sidelink_pc5s_element));
         memcpy(&sl_pc5s_msg_send->pdcp_data_header, pdcp_data_header,sizeof(pdcp_data_header_t));
         memcpy(&sl_pc5s_msg_send->pc5sPrimitive.pc5_signalling_message, pc5_signalling_message,sizeof(PC5SignallingMessage));

         printf("Send pc5_signalling_message\n");
         printf("Send pc5_signalling_message, pdcp header traffic_type: %d \n", pdcp_data_header->traffic_type);
         printf("Send pc5_signalling_message, pdcp header rb_id: %d \n", pdcp_data_header->rb_id);
         printf("Send pc5_signalling_message, pdcp header data_size: %d \n", pdcp_data_header->data_size);
         printf("Send pc5_signalling_message, pdcp header inst: %d \n", pdcp_data_header->inst);
         printf("Send pc5_signalling_message, pdcp header source L2Id 0x%08x \n", pdcp_data_header->sourceL2Id);
         printf("Send pc5_signalling_message, pdcp header destination L2Id 0x%08x \n", pdcp_data_header->destinationL2Id);

         send_pc5s_message(sl_pc5s_msg_send);
      }
      if (receiver_mode){ //For receiver

         uint32_t source_id = 0x654321; //destination from Sender's point of view
         uint8_t type = 0; //rx

         //for the moment, we use only one SLRB_ID for PC5S signaling (instead of using 28,29,30)
         status = setup_PC5s_request(type, source_id, 0x00000000);
         if (status > -1 ){
            printf("[PC5S] SRLBs have been established for PC5S\n");
         } else{
            printf("[PC5S] Failed to establish SLRBs for PC5S\n");
         }
      }

      //for both sender and receiver
      // get the reply from OAI
      memset(receive_buf, 0, BUFSIZE);
      serverlen = sizeof(pdcp_sin);
      sl_pc5s_msg_rcv = calloc(1, sizeof(sidelink_pc5s_element));
      while (1){
         n = recvfrom(pdcp_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&pdcp_sin, &serverlen);
         if (n < 0)
            error("ERROR: Failed to receive from OAI");

         memcpy((void *)sl_pc5s_msg_rcv, (void *)receive_buf, sizeof(sidelink_pc5s_element));
         printf("------------------------------------------------\n");
         printf("Received on socket from OAI, traffic_type: %d\n",sl_pc5s_msg_rcv->pdcp_data_header.traffic_type);
         printf("Received on socket from OAI, rb_id: %d\n",sl_pc5s_msg_rcv->pdcp_data_header.rb_id);
         printf("Received on socket from OAI, inst: %d\n",sl_pc5s_msg_rcv->pdcp_data_header.inst);
         printf("Received on socket from OAI, data_size: %d\n",sl_pc5s_msg_rcv->pdcp_data_header.data_size);
         if (sl_pc5s_msg_rcv->pdcp_data_header.traffic_type == TRAFFIC_PC5S_SIGNALLING) {
            printf("Received on socket from OAI, PC5S message\n");
         }
      }
   }

   else if (onetomany_scenario) { //One-to-many scenario

      // Send group establishment request to OAI [RX/TX]
      uint8_t type = 1;
      uint32_t group_ip_address = inet_addr("224.0.0.3");
      printf("group_ip_address "IPV4_ADDR" \n", IPV4_ADDR_FORMAT(group_ip_address));
      uint32_t source_id = 0x01; //hardcoded
      uint32_t group_id = 0x02;  //should be 0x02 in case of one-to-one (hardcoded)
      uint8_t pppp = 2;

      slrb_id  = setup_PC5u_group_request (type, source_id, group_id, group_ip_address, pppp);
      if (slrb_id > 0) {
         printf("PC5u has been established with SLRB_ID %i\n",slrb_id);
      } else {
         printf("PC5u cannot be established\n");
      }

      //create thread to receive the status notification
      //  pthread_t ue_state_notification_thread;
      //  if( pthread_create( &ue_state_notification_thread , NULL ,  rcv_UE_status_notification , (void*) NULL) < 0)
      //     printf("ERROR: could not create thread");

      //create thread to send ping
      pthread_t generate_traffic_thread;
      if( pthread_create( &generate_traffic_thread , NULL ,  generate_traffic , (void*) NULL) < 0)
         printf("ERROR: could not create thread");

      int count = 0;
      while (1) {
         printf("------------------------------------------------\n");
         printf("Listening to incoming message from OAI.... \n");

         memset(receive_buf, 0, BUFSIZE);
         serverlen = sizeof(ctrl_sin);
         n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
         if (n < 0)
            error("ERROR: Failed to receive from OAI");

         sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
         if (sl_ctrl_msg->type == UE_STATUS_INFO) {
            printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
            status =  sl_ctrl_msg->sidelinkPrimitive.ue_state;
            if (status == UE_STATE_ON_NETWORK ) {
               printf("[UEStatusInformation] UE is on coverage\n");
            } else if (status == UE_STATE_OFF_NETWORK) {
               printf("[UEStatusInformation] UE is out of coverage\n");
            }
         }
         count++;
         if (count == 1) {
            //Send a group communication release request to OAI
            slrb_id = SLRB_TO_BE_RELEASED;
            status  = release_PC5u_group_request (slrb_id);
            if (status == GROUP_COMMUNICATION_RELEASE_OK) {
               printf("SLRB with ID %i has been released\n",slrb_id);
            } else {
               printf("SLRD with ID %i cannot be released\n", slrb_id);
            }
            break;
         }
      }

   }

   return 0;
}


//--------------------------------------------------------
int session_init()
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send SessionInitializationRequest message to OAI (msg type: %d )\n",SESSION_INIT_REQ);
   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = SESSION_INIT_REQ;
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the initialization message to the OAI
   int serverlen = sizeof(ctrl_sin);
   n = sendto(ctrl_sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&ctrl_sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(ctrl_sin);
   n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   if (sl_ctrl_msg->type == UE_STATUS_INFO) {
      printf("Connection with OAI is established successfully!\n");
      printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
      printf("UEStatusInformation, Status: %d\n",sl_ctrl_msg->sidelinkPrimitive.ue_state);
      return sl_ctrl_msg->sidelinkPrimitive.ue_state;
   }
   return 0;
}

//--------------------------------------------------------
int setup_PC5u_group_request(uint8_t type, uint32_t source_id, uint32_t group_id, uint32_t group_ip_address, uint8_t pppp)
//int setup_PC5u_group_request(uint32_t source_id, uint32_t group_id, uint8_t pppp)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   struct GroupCommunicationEstablishReq *group_comm_establish_req = NULL;
   int slrb_id;

   printf("------------------------------------------------\n");
   printf("Send GroupCommunicationEstablishReq message to OAI (msg type: %d )\n",GROUP_COMMUNICATION_ESTABLISH_REQ);
   memset(send_buf,0,BUFSIZE);
   //fill the message
   group_comm_establish_req = calloc(1, sizeof(struct GroupCommunicationEstablishReq));
   group_comm_establish_req->type = type;
   group_comm_establish_req->sourceL2Id = source_id;
   group_comm_establish_req->groupL2Id = group_id;
   group_comm_establish_req->groupIpAddress = group_ip_address;
   group_comm_establish_req->pppp = pppp;

   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = GROUP_COMMUNICATION_ESTABLISH_REQ;
   memcpy(&sl_ctrl_msg->sidelinkPrimitive.group_comm_establish_req, group_comm_establish_req,sizeof(struct GroupCommunicationEstablishReq));
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the message to OAI
   int serverlen = sizeof(ctrl_sin);
   int n = sendto(ctrl_sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&ctrl_sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(ctrl_sin);
   n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   printf("Received on socket from OAI, msg type: %d\n",sl_ctrl_msg->type);
   if (sl_ctrl_msg->type == GROUP_COMMUNICATION_ESTABLISH_RSP) {
      slrb_id = sl_ctrl_msg->sidelinkPrimitive.slrb_id;
      printf("Received GroupCommunicationEstablishResponse on socket from OAI, slrb %d\n",slrb_id);
      return slrb_id;
   } else {
      return -1;
   }
}

//--------------------------------------------------------
int release_PC5u_group_request(uint8_t slrb_id)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   int status;
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send GroupCommunicationReleaseRequest message to OAI (msg type: %d )\n",GROUP_COMMUNICATION_RELEASE_REQ);
   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = GROUP_COMMUNICATION_RELEASE_REQ;
   sl_ctrl_msg->sidelinkPrimitive.slrb_id = slrb_id;
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the GroupCommunicationReleaseRequest message to the OAI
   int serverlen = sizeof(ctrl_sin);
   n = sendto(ctrl_sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&ctrl_sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(ctrl_sin);
   n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   if (sl_ctrl_msg->type == GROUP_COMMUNICATION_RELEASE_RSP) {
      printf("Received GroupCommunicationReleaseResponse on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
      status = sl_ctrl_msg->sidelinkPrimitive.group_comm_release_rsp;
      printf("GroupCommunicationReleaseResponse, Status: %d\n", status);
      return status;
   } else {
      return -1;
   }

   return 0;
}

//--------------------------------------------------------
void *rcv_UE_status_notification(void *sock)
{
   char receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int serverlen, n, status;

   printf("------------------------------------------------\n");
   printf("Thread to receive UEStateInformation from OAI\n");
   printf("Listening to incoming message from OAI.... \n");


   while (1) {
      memset(receive_buf, 0, BUFSIZE);
      serverlen = sizeof(ctrl_sin);
      n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
      if (n < 0)
         error("ERROR: Failed to receive from OAI");

      sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
      if (sl_ctrl_msg->type == UE_STATUS_INFO) {
         printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
         status =  sl_ctrl_msg->sidelinkPrimitive.ue_state;
         if (status == UE_STATE_ON_NETWORK ) {
            printf("[UEStatusInformation] UE is on coverage\n");
         } else if (status == UE_STATE_OFF_NETWORK) {
            printf("[UEStatusInformation] UE is out of coverage\n");
         }
      }
   }

   return 0;
}


//--------------------------------------------------------
void *generate_traffic()
{
   //generate PC5-U packet
   //system("ping 224.0.0.1 -I oip0 -c 100");
   //system("ping -b 10.0.0.255 -I oip0 -c 10"); //for broadcast
   //system("ping 10.0.0.2 -I oip0 -m 1 -c 10"); //for unicast traffic
   //system("iperf -c 224.0.0.1 -u -b 0.1M --bind 10.0.0.1 -t 100"); //iperf - multicast
   return 0;
}

//--------------------------------------------------------
// error - wrapper for perror
void error(char *msg) {
   perror(msg);
   exit(1);
}

//--------------------------------------------------------
int    pdcp_session_init()
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   sidelink_pc5s_element *sl_pc5s_msg = NULL;
   pdcp_data_header_t *pdcp_data_header = NULL;
   int slrb_id;

   printf("------------------------------------------------\n");
   printf("Initiate a PDCP session \n");
   memset(send_buf,0,BUFSIZE);

   //fill PDCP header
   pdcp_data_header = calloc(1, sizeof(pdcp_data_header_t));
   pdcp_data_header->traffic_type = TRAFFIC_PC5S_SESSION_INIT; //init a new session
   pdcp_data_header->rb_id = 0;
   pdcp_data_header->data_size = 0;
   pdcp_data_header->inst = 0;

   sl_pc5s_msg = calloc(1, sizeof(sidelink_pc5s_element));
   memcpy(&sl_pc5s_msg->pdcp_data_header, pdcp_data_header,sizeof(pdcp_data_header_t));
   memcpy((void *)send_buf, (void *)sl_pc5s_msg, sizeof(sidelink_pc5s_element));

   // send the message to OAI
   int serverlen = sizeof(pdcp_sin);
   int n = sendto(pdcp_sockfd, (char *)send_buf, sizeof(sidelink_pc5s_element), 0, (struct sockaddr *)&pdcp_sin, serverlen);

   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(pdcp_sin);
   n = recvfrom(pdcp_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&pdcp_sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_pc5s_msg = (sidelink_pc5s_element *) receive_buf;
   if (sl_pc5s_msg->pdcp_data_header.traffic_type == TRAFFIC_PC5S_SESSION_INIT) {
      if (sl_pc5s_msg->pc5sPrimitive.status == 1)
         printf("A PDCP session has been established successfully! \n");
      return sl_pc5s_msg->pc5sPrimitive.status;
   }
   return 0;
}

//--------------------------------------------------------
int  send_pc5s_message(sidelink_pc5s_element *sl_pc5s_msg )
{
   char send_buf[BUFSIZE];

   memcpy((void *)send_buf, (void *)sl_pc5s_msg, sizeof(sidelink_pc5s_element));
   // send the message to PDCP
   int serverlen = sizeof(pdcp_sin);
   int n = sendto(pdcp_sockfd, (char *)send_buf, sizeof(sidelink_pc5s_element), 0, (struct sockaddr *)&pdcp_sin, serverlen);

   if (n < 0)
      error("ERROR: Failed to send to OAI/PDCP");

}


//--------------------------------------------------------
int setup_PC5s_request(uint8_t type,  uint32_t source_id, uint32_t destination_id)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   struct PC5SEstablishReq *pc5s_establish_req = NULL;
   struct PC5SEstablishRsp *pc5s_establish_rsp = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send PC5SEstablishReq message to OAI (msg type: %d )\n",PC5S_ESTABLISH_REQ);
   memset(send_buf,0,BUFSIZE);
   //fill the message
   pc5s_establish_req = calloc(1, sizeof(struct PC5SEstablishReq));
   pc5s_establish_req->type = type;
   pc5s_establish_req->sourceL2Id = source_id;
   pc5s_establish_req->destinationL2Id = destination_id;

   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = PC5S_ESTABLISH_REQ;
   memcpy(&sl_ctrl_msg->sidelinkPrimitive.pc5s_establish_req, pc5s_establish_req,sizeof(struct PC5SEstablishReq));
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the message to OAI
   int serverlen = sizeof(ctrl_sin);
   n = sendto(ctrl_sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&ctrl_sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(ctrl_sin);
   n = recvfrom(ctrl_sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&ctrl_sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   printf("Received on socket from OAI, msg type: %d\n",sl_ctrl_msg->type);
   if (sl_ctrl_msg->type == PC5S_ESTABLISH_RSP) {
      printf("[PC5S] SRLBs %d, %d, %d have been established for PC5S\n",sl_ctrl_msg->sidelinkPrimitive.pc5s_establish_rsp.slrbid_lcid28, sl_ctrl_msg->sidelinkPrimitive.pc5s_establish_rsp.slrbid_lcid29, sl_ctrl_msg->sidelinkPrimitive.pc5s_establish_rsp.slrbid_lcid30 );
      return 0;
   }
   else return -1;
}


//--------------------------------------------------------
int  send_PC5D_message(PC5DiscoveryMessage *pc5_discovery_message)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send PC5D message to OAI (msg type: %d )\n",PC5_DISCOVERY_MESSAGE);
   memset(send_buf,0,BUFSIZE);
   //fill the message
   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = PC5_DISCOVERY_MESSAGE;
   memcpy(&sl_ctrl_msg->sidelinkPrimitive.pc5_discovery_message, pc5_discovery_message,sizeof(PC5DiscoveryMessage));
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the message to OAI
   int serverlen = sizeof(ctrl_sin);
   n = sendto(ctrl_sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&ctrl_sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

}

