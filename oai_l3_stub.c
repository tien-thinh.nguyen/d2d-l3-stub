/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file oai_l3_stub.c
 * \brief An example of how to exchange messages between OAI and Velcore App via a control socket
 * \author Tien-Thinh NGUYEN
 * \date 20/11/2017
 * \version 0.1
 * \email:tien-thinh.nguyen@eurecom.fr
 * \company Eurecom
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include<pthread.h>
#include <unistd.h>
#include <linux/netlink.h>
#include <stdint.h>

#include "oai_l3_stub.h"

//global variables
struct sockaddr_in clientaddr;
int slrb_id;
pthread_mutex_t slrb_mutex;

//--------------------------------------------------
int main(int argc, char **argv) {
   int sockfd;
   int sockfd_netlink;
   int clientlen;
   struct sockaddr_in serveraddr;
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   int optval; // flag value for setsockopt
   int n; // message byte size
   struct sidelink_ctrl_element *sl_ctrl_msg_recv = NULL;
   struct sidelink_ctrl_element *sl_ctrl_msg_send = NULL;
   //for Netlink
   struct sockaddr_nl src_addr;
   struct nlmsghdr *nlh_send = NULL;
   struct nlmsghdr *nlh_rcv = NULL;
   struct msghdr msg_rcv, msg_send;
   char nl_rx_buf[MAX_PAYLOAD];
   struct iovec iov_rcv, iov_send;

   //init the mutex
   pthread_mutex_init(&slrb_mutex, NULL);

   // create the control socket
   sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (sockfd < 0)
      error("ERROR: Failed on opening socket");
   optval = 1;
   setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
         (const void *)&optval , sizeof(int));
   //build the server's  address
   bzero((char *) &serveraddr, sizeof(serveraddr));
   serveraddr.sin_family = AF_INET;
   serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
   serveraddr.sin_port = htons(CONTROL_SOCKET_PORT_NO);
   // associate the parent socket with a port
   if (bind(sockfd, (struct sockaddr *) &serveraddr,
         sizeof(serveraddr)) < 0)
      error("ERROR: Failed on binding the socket");

   //create a netlink socket
   sockfd_netlink = socket(PF_NETLINK, SOCK_RAW, UE_IP_PDCP_NETLINK_ID);
   if(sockfd_netlink<0) {
      printf("Sending message\n");
      return -1;
   }
   memset(&src_addr, 0, sizeof(src_addr));
   src_addr.nl_family = AF_NETLINK;
   src_addr.nl_pid = PDCP_PID;
   bind(sockfd_netlink, (struct sockaddr*)&src_addr, sizeof(src_addr));

   //create thread to listen to netlink socket
   pthread_t netlink_socket_thread;
   if( pthread_create( &netlink_socket_thread, NULL, listen_netlink_socket_pdcp, (void*) &sockfd_netlink) < 0)
      error("ERROR: could not create thread");

   //from the main program, listen for the incoming messages from control socket (ProSe App)
   clientlen = sizeof(clientaddr);
   int enable_notification = 1;
   while (1) {

      printf("------------------------------------------------\n");
      printf("Listening to incoming connection from ProSe App  \n");
      // receive a message from ProSe App
      memset(receive_buf, 0, BUFSIZE);
      n = recvfrom(sockfd, receive_buf, BUFSIZE, 0,
            (struct sockaddr *) &clientaddr, &clientlen);
      if (n < 0)
         error("ERROR: Failed to receive from ProSe App");

      //sl_ctrl_msg_recv = (struct sidelink_ctrl_element *) receive_buf;
      sl_ctrl_msg_recv = calloc(1, sizeof(struct sidelink_ctrl_element));
      memcpy((void *)sl_ctrl_msg_recv, (void *)receive_buf, sizeof(struct sidelink_ctrl_element));
      clientlen = sizeof(clientaddr);

      //process the message
      switch (sl_ctrl_msg_recv->type) {
      case SESSION_INIT_REQ:
#ifdef DEBUG_CTRL_SOCKET
         printf("Received SessionInitializationRequest on socket from ProSe App (msg type: %d)\n",sl_ctrl_msg_recv->type);
#endif

         printf("------------------------------------------------\n");
         printf("Send UEStateInformation to ProSe App  \n");
         memset(send_buf, 0, BUFSIZE);
         sl_ctrl_msg_send = calloc(1, sizeof(struct sidelink_ctrl_element));
         sl_ctrl_msg_send->type = UE_STATUS_INFO;
         sl_ctrl_msg_send->sidelinkPrimitive.ue_state = UE_STATE_OFF_NETWORK; //off-network
         memcpy((void *)send_buf, (void *)sl_ctrl_msg_send, sizeof(struct sidelink_ctrl_element));
         free(sl_ctrl_msg_send);
         n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&clientaddr, clientlen);
         if (n < 0)
            error("ERROR: Failed to send to ProSe App");

#ifdef DEBUG_CTRL_SOCKET
         struct sidelink_ctrl_element *ptr_ctrl_msg = NULL;
         ptr_ctrl_msg = (struct sidelink_ctrl_element *) send_buf;
         printf("[UEStateInformation] msg type: %d\n",ptr_ctrl_msg->type);
         printf("[UEStateInformation] UE state: %d\n",ptr_ctrl_msg->sidelinkPrimitive.ue_state);
#endif

         if (enable_notification > 0) {
            //create thread to send status notification (for testing purpose, status notification will be sent e.g., every 20 seconds)
            pthread_t notification_thread;
            if( pthread_create( &notification_thread , NULL ,  send_UE_status_notification , (void*) &sockfd) < 0)
               error("ERROR: could not create thread");
         }
         enable_notification = 0;
         break;

      case GROUP_COMMUNICATION_ESTABLISH_REQ:
         printf("------------------------------------------------\n");
#ifdef DEBUG_CTRL_SOCKET
         printf("Received GroupCommunicationEstablishReq on socket from ProSe App (msg type: %d)\n",sl_ctrl_msg_recv->type);
         printf("[GroupCommunicationEstablishReq] type: %d\n",sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.type);
         printf("[GroupCommunicationEstablishReq] source Id: %d\n",sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.sourceL2Id);
         printf("[GroupCommunicationEstablishReq] group Id: %d\n",sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.groupL2Id);
         printf("[GroupCommunicationEstablishReq] group IP Address: " IPV4_ADDR "\n",IPV4_ADDR_FORMAT(sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.groupIpAddress));
#endif
         printf("Send GroupCommunicationEstablishResp to ProSe App \n");
         memset(send_buf, 0, BUFSIZE);
         sl_ctrl_msg_send = calloc(1, sizeof(struct sidelink_ctrl_element));
         sl_ctrl_msg_send->type = GROUP_COMMUNICATION_ESTABLISH_RSP;
         //in case of TX, assign a new SLRB and prepare for the filter
         if (sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.type == 1) {
#ifdef DEBUG_CTRL_SOCKET
            printf("[GroupCommunicationEstablishReq] PPPP: %d\n",sl_ctrl_msg_recv->sidelinkPrimitive.group_comm_establish_req.pppp);
#endif
            sl_ctrl_msg_send->sidelinkPrimitive.slrb_id = SLRB_ID; //slrb_id
            pthread_mutex_lock(&slrb_mutex);
            slrb_id = SLRB_ID;
            pthread_mutex_unlock(&slrb_mutex);
         } else{ //RX
            sl_ctrl_msg_send->sidelinkPrimitive.slrb_id = SL_DEFAULT_RAB_ID;
         }
         memcpy((void *)send_buf, (void *)sl_ctrl_msg_send, sizeof(struct sidelink_ctrl_element));
         free(sl_ctrl_msg_send);
         n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&clientaddr, clientlen);
         if (n < 0)
            error("ERROR: Failed to send to ProSe App");

#ifdef DEBUG_CTRL_SOCKET
         ptr_ctrl_msg = (struct sidelink_ctrl_element *) send_buf;
         printf("[GroupCommunicationEstablishResponse] msg type: %d\n",ptr_ctrl_msg->type);
         printf("[GroupCommunicationEstablishResponse] slrb_id: %d\n",ptr_ctrl_msg->sidelinkPrimitive.slrb_id);
#endif
         break;
      case GROUP_COMMUNICATION_RELEASE_REQ:
         printf("-----------------------------------\n");
#ifdef DEBUG_CTRL_SOCKET
         printf("Received GroupCommunicationReleaseRequest on socket from ProSe App (msg type: %d)\n",sl_ctrl_msg_recv->type);
         printf("[GroupCommunicationReleaseRequest] Slrb Id: %i\n",sl_ctrl_msg_recv->sidelinkPrimitive.slrb_id);
#endif
         printf("Send GroupCommunicationReleaseResponse to ProSe App \n");
         memset(send_buf, 0, BUFSIZE);

         sl_ctrl_msg_send = calloc(1, sizeof(struct sidelink_ctrl_element));
         sl_ctrl_msg_send->type = GROUP_COMMUNICATION_RELEASE_RSP;
         //if the requested id exists -> release this ID
         if (sl_ctrl_msg_recv->sidelinkPrimitive.slrb_id == slrb_id) {
            sl_ctrl_msg_send->sidelinkPrimitive.group_comm_release_rsp = GROUP_COMMUNICATION_RELEASE_OK;
            pthread_mutex_lock(&slrb_mutex);
            slrb_id = 0; //Reset slrb_id
            pthread_mutex_unlock(&slrb_mutex);
         } else {
            sl_ctrl_msg_send->sidelinkPrimitive.group_comm_release_rsp = GROUP_COMMUNICATION_RELEASE_FAILURE;
         }
         memcpy((void *)send_buf, (void *)sl_ctrl_msg_send, sizeof(struct sidelink_ctrl_element));
         free(sl_ctrl_msg_send);
         n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&clientaddr, clientlen);
         if (n < 0)
            error("ERROR: Failed to send to ProSe App");
         break;

      default:
         break;
      }
      free(sl_ctrl_msg_recv);
   }

}

//--------------------------------------------------------
void *send_UE_status_notification(void *sockfd)
{
   //Get the socket descriptor
   int sock = *(int*)sockfd;
   int read_size;
   char *message , client_message[2000];
   char send_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_message_send = NULL;
   struct sockaddr_in serveraddr;

   while (1) {
      //sleep UE_STATE_NOTIFICATION_INTERVAL seconds
      sleep(UE_STATE_NOTIFICATION_INTERVAL);
      printf("------------------------------------------------\n");
      printf("Thread to send UEStateInformation every %i seconds\n", UE_STATE_NOTIFICATION_INTERVAL);
      printf("Send UEStateInformation to ProSe App  \n");
      memset(send_buf, 0, BUFSIZE);

      sl_ctrl_message_send = calloc(1, sizeof(struct sidelink_ctrl_element));
      sl_ctrl_message_send->type = UE_STATUS_INFO;
      sl_ctrl_message_send->sidelinkPrimitive.ue_state = UE_STATE_ON_NETWORK; //on-network
      memcpy((void *)send_buf, (void *)sl_ctrl_message_send, sizeof(struct sidelink_ctrl_element));

      int clientlen = sizeof(clientaddr);
      int n = sendto(sock, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&clientaddr, clientlen);
      if (n < 0)
         error("ERROR: Failed to send to ProSe App");
   }

   return 0;
}

//--------------------------------------------------------
// error - wrapper for perror
void error(char *msg) {
   perror(msg);
   exit(1);
}

//--------------------------------------
void *listen_netlink_socket_pdcp(void *sockfd_netlink)
{
   struct sockaddr_nl dest_addr;
   struct nlmsghdr *nlh_send = NULL;
   struct nlmsghdr *nlh_rcv = NULL;
   struct msghdr msg_rcv, msg_send, msg;
   char nl_rx_buf[MAX_PAYLOAD];
   struct iovec iov_rcv, iov_send;
   int j;

   //Get the socket descriptor
   int sock = *(int*)sockfd_netlink;

   memset(&dest_addr, 0, sizeof(dest_addr));
   memset(&dest_addr, 0, sizeof(dest_addr));
   dest_addr.nl_family = AF_NETLINK;
   dest_addr.nl_pid = 0; /* For Linux Kernel */
   dest_addr.nl_groups = 0; /* unicast */

   nlh_rcv = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD));
   memset(nlh_rcv, 0, NLMSG_SPACE(MAX_PAYLOAD));
   nlh_rcv->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
   nlh_rcv->nlmsg_pid = 1;
   nlh_rcv->nlmsg_flags = 0;

   memset(nl_rx_buf, 0, MAX_PAYLOAD);
   iov_rcv.iov_base = (void *)nlh_rcv;
   iov_rcv.iov_len = nlh_rcv->nlmsg_len;
   msg_rcv.msg_name = (void *)&dest_addr;
   msg_rcv.msg_namelen = sizeof(dest_addr);
   msg_rcv.msg_iov = &iov_rcv;
   msg_rcv.msg_iovlen = 1;
   msg.msg_name = (void *)&dest_addr;
   msg.msg_namelen = sizeof(dest_addr);

   pdcp_data_req_header_t pdcp_header;
   nlh_send = (struct nlmsghdr *)calloc(1, NLMSG_SPACE(MAX_PAYLOAD));
   memset(nlh_send, 0, NLMSG_SPACE(MAX_PAYLOAD));
   nlh_send->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD);
   nlh_send->nlmsg_pid = 1;
   nlh_send->nlmsg_flags = 0;

   //listen to incoming packets from UE_IP.ko and reply accordingly
   while(1){

      // Read message from UE_IP.ko
      int len= recvmsg(sock, &msg_rcv, 0);


#ifdef DEBUG_PDCP
      printf("Received socket with length %d, %d \n",len,nlh_rcv->nlmsg_len);
      for (j=0; j<nlh_rcv->nlmsg_len; j++) {
         printf("%2x ",((unsigned char *)(NLMSG_DATA(nlh_rcv)))[j]);
      }
      printf("\n");

#endif

      //store the header for the next packet (real data)
      if (nlh_rcv->nlmsg_len == (NETLINK_HEADER_SIZE + UE_IP_PDCPH_SIZE)) {
         memcpy((char *)nl_rx_buf, (char *)NLMSG_DATA(nlh_rcv), UE_IP_PDCPH_SIZE);
         memcpy((char *)&pdcp_header, (char *)NLMSG_DATA(nlh_rcv), UE_IP_PDCPH_SIZE);
         printf("Received socket with rb_id %d\n",pdcp_header.rb_id);

#ifdef DEBUG_PDCP
         printf("Received socket with data_size %d\n",pdcp_header.data_size);
         printf("Received socket with inst %d\n",pdcp_header.inst);
#endif
      } else{
         //send back to the same interface
         //memcpy(NLMSG_DATA(nlh_send), (char *)nl_rx_buf, UE_IP_PDCPH_SIZE);
         //send to a different instance -> different interface
         pdcp_header.inst = 1;

         memcpy(NLMSG_DATA(nlh_send), (char *)&pdcp_header, UE_IP_PDCPH_SIZE);
         memcpy(NLMSG_DATA(nlh_send) + UE_IP_PDCPH_SIZE,  (char*) NLMSG_DATA(nlh_rcv), nlh_rcv->nlmsg_len);
         iov_send.iov_base = (void *)nlh_send;
         iov_send.iov_len = nlh_rcv->nlmsg_len+UE_IP_PDCPH_SIZE;
         msg_send.msg_name = (void *)&dest_addr;
         msg_send.msg_namelen = sizeof(dest_addr);
         msg_send.msg_iov = &iov_send;
         msg_send.msg_iovlen = 1;

#ifdef DEBUG_PDCP
         printf("SEND packet with length %d, %d \n",len,nlh_rcv->nlmsg_len);
         for (j=0; j<nlh_rcv->nlmsg_len; j++) {
            printf("%2x ",((unsigned char *)(NLMSG_DATA(nlh_send)))[j]);
         }
         printf("\n");
#endif

         int slrbid;
         pthread_mutex_lock(&slrb_mutex);
         slrbid = slrb_id;
         pthread_mutex_unlock(&slrb_mutex);

         //filter the packet according to the SLRB_ID
         if ((pdcp_header.rb_id == slrbid) && (slrbid > 0)) {
            sendmsg(sock,&msg_send,0);
            printf("Send back to the UE_IP.ko\n");

         } else {
            printf("SLRB is not configured, drop the packet\n");
            printf("\n");
         }
      }

   }
   close(sock);
   return 0;
}
