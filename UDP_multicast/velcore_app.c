/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file velcore_app_udp.c
 * \brief An example of how to exchange messages between OAI and Velcore App via a control socket
 * \author Tien-Thinh NGUYEN
 * \date 20/11/2017
 * \version 0.1
 * \email:tien-thinh.nguyen@eurecom.fr
 * \company Eurecom
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include<pthread.h>
#include <arpa/inet.h>

#define IPV4_ADDR    "%u.%u.%u.%u"
#define IPV4_ADDR_FORMAT(aDDRESS)                 \
      (uint8_t)((aDDRESS)  & 0x000000ff),         \
      (uint8_t)(((aDDRESS) & 0x0000ff00) >> 8 ),  \
      (uint8_t)(((aDDRESS) & 0x00ff0000) >> 16),  \
      (uint8_t)(((aDDRESS) & 0xff000000) >> 24)

#define DEBUG
#define BUFSIZE 1024
#define CONTROL_SOCKET_PORT_NO 8888
#define SLRB_TO_BE_RELEASED 11

//Primitives
#define SESSION_INIT_REQ                    1
#define UE_STATUS_INFO                      2
#define GROUP_COMMUNICATION_ESTABLISH_REQ   3
#define GROUP_COMMUNICATION_ESTABLISH_RSP   4
#define DIRECT_COMMUNICATION_ESTABLISH_REQ  5
#define DIRECT_COMMUNICATION_ESTABLISH_RSP  6
#define GROUP_COMMUNICATION_RELEASE_REQ     7
#define GROUP_COMMUNICATION_RELEASE_RSP     8

typedef enum {
   UE_STATE_OFF_NETWORK,
   UE_STATE_ON_NETWORK
} UE_STATE_t;

typedef enum {
   GROUP_COMMUNICATION_RELEASE_OK = 0,
   GROUP_COMMUNICATION_RELEASE_FAILURE
} Group_Communication_Status_t;

struct GroupCommunicationEstablishReq {
   uint8_t type; //0 - rx, 1 - tx
   uint32_t sourceL2Id;
   uint32_t groupL2Id;
   uint32_t groupIpAddress;
   uint8_t pppp;
};

struct DirectCommunicationEstablishReq {
   uint32_t sourceL2Id;
   uint32_t destinationL2Id;
};

struct sidelink_ctrl_element {
   unsigned short type;
   union {
      struct GroupCommunicationEstablishReq group_comm_establish_req;
      struct DirectCommunicationEstablishReq direct_comm_estblish_req;
      Group_Communication_Status_t group_comm_release_rsp;
      //struct DirectCommunicationReleaseReq  direct_comm_release_req;
      UE_STATE_t ue_state;
      int slrb_id;

   } sidelinkPrimitive;
};

//global variables
extern int sockfd;
extern struct sockaddr_in sin;

//global variables
int sockfd;
struct sockaddr_in sin;

/**
\brief Establish a connection with OAI
@return status of the UE*/
int session_init();

/**
\brief Send a request to establish a SLRB for a group communication
@param type 0 - for RX, 1 for TX
@param source_id Layer2 ID of the source
@param group_id Group layer2 ID
@param pppp Represent packet-per-priority
@return SLRB ID Established for this communication*/
//int setup_PC5u_group_request(uint32_t source_id, uint32_t group_id, uint8_t pppp);
int setup_PC5u_group_request(uint8_t type, uint32_t source_id, uint32_t group_id, uint32_t group_ip_address, uint8_t pppp);

/**
\brief Send a request to release a SLRB for a group communication
@param SLRB ID Used for this communication
@returns 0 on success, -1 on failure*/
int release_PC5u_group_request(uint8_t slrb_id);

// a thread function to receive UE status notification
void *rcv_UE_status_notification(void *);

// a thread function to receive UE status notification
void *generate_traffic();
void error(char *msg);

//--------------------------------------------------------
int main(int argc, char **argv) {

   int serverlen;
   struct hostent *server;
   char *hostname;
   char receive_buf[BUFSIZE];
   int status;
   int slrb_id;
   int n;
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;

   hostname = "localhost";
   // create the socket
   sockfd = socket(AF_INET, SOCK_DGRAM, 0);
   if (sockfd < 0)
      error("ERROR: Failed to create new socket");
   server = gethostbyname(hostname);
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host as %s\n", hostname);
      exit(0);
   }
   // build the server's address
   memset(&sin, 0, sizeof(struct sockaddr_in));
   sin.sin_family = AF_INET;
   bcopy((char *)server->h_addr,
         (char *)&sin.sin_addr.s_addr, server->h_length);
   sin.sin_port = htons(CONTROL_SOCKET_PORT_NO);

   //Init a session with OAI
   status = session_init ();
   if (status == UE_STATE_ON_NETWORK ) {
      printf("[UEStatusInformation] UE is on coverage\n");
   } else if (status == UE_STATE_OFF_NETWORK) {
      printf("[UEStatusInformation] UE is out of coverage\n");
   }

   // Send group establishment request to OAI [TX]
   uint8_t type = 1;
   uint32_t group_ip_address = inet_addr("224.0.0.3");
   printf("group_ip_address "IPV4_ADDR" \n", IPV4_ADDR_FORMAT(group_ip_address));
   uint32_t source_id = 111;
   uint32_t group_id = 2222;
   uint8_t pppp = 2;

   slrb_id  = setup_PC5u_group_request (type, source_id, group_id, group_ip_address, pppp);
   if (slrb_id > 0) {
      printf("PC5u has been established with SLRB_ID %i\n",slrb_id);
   } else {
      printf("PC5u cannot be established\n");
   }

   //create thread to receive the status notification
   //  pthread_t ue_state_notification_thread;
   //  if( pthread_create( &ue_state_notification_thread , NULL ,  rcv_UE_status_notification , (void*) NULL) < 0)
   //     printf("ERROR: could not create thread");

   //create thread to send ping
   pthread_t generate_traffic_thread;
   if( pthread_create( &generate_traffic_thread , NULL ,  generate_traffic , (void*) NULL) < 0)
      printf("ERROR: could not create thread");

   int count = 0;
   while (1) {
      printf("------------------------------------------------\n");
      printf("Listening to incoming message from OAI.... \n");

      memset(receive_buf, 0, BUFSIZE);
      serverlen = sizeof(sin);
      n = recvfrom(sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&sin, &serverlen);
      if (n < 0)
         error("ERROR: Failed to receive from OAI");

      sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
      if (sl_ctrl_msg->type == UE_STATUS_INFO) {
         printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
         status =  sl_ctrl_msg->sidelinkPrimitive.ue_state;
         if (status == UE_STATE_ON_NETWORK ) {
            printf("[UEStatusInformation] UE is on coverage\n");
         } else if (status == UE_STATE_OFF_NETWORK) {
            printf("[UEStatusInformation] UE is out of coverage\n");
         }
      }
      //count++;
      if (count == 1) {
         //Send a group communication release request to OAI
         slrb_id = SLRB_TO_BE_RELEASED;
         status  = release_PC5u_group_request (slrb_id);
         if (status == GROUP_COMMUNICATION_RELEASE_OK) {
            printf("SLRB with ID %i has been released\n",slrb_id);
         } else {
            printf("SLRD with ID %i cannot be released\n", slrb_id);
         }
         break;
      }
   }

   return 0;
}


//--------------------------------------------------------
int session_init()
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send SessionInitializationRequest message to OAI (msg type: %d )\n",SESSION_INIT_REQ);
   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = SESSION_INIT_REQ;
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the initialization message to the OAI
   int serverlen = sizeof(sin);
   n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(sin);
   n = recvfrom(sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   if (sl_ctrl_msg->type == UE_STATUS_INFO) {
      printf("Connection with OAI is established successfully!\n");
      printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
      printf("UEStatusInformation, Status: %d\n",sl_ctrl_msg->sidelinkPrimitive.ue_state);
      return sl_ctrl_msg->sidelinkPrimitive.ue_state;
   }
   return 0;
}

//--------------------------------------------------------
int setup_PC5u_group_request(uint8_t type, uint32_t source_id, uint32_t group_id, uint32_t group_ip_address, uint8_t pppp)
//int setup_PC5u_group_request(uint32_t source_id, uint32_t group_id, uint8_t pppp)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   struct GroupCommunicationEstablishReq *group_comm_establish_req = NULL;
   int slrb_id;

   printf("------------------------------------------------\n");
   printf("Send GroupCommunicationEstablishReq message to OAI (msg type: %d )\n",GROUP_COMMUNICATION_ESTABLISH_REQ);
   memset(send_buf,0,BUFSIZE);
   //fill the message
   group_comm_establish_req = calloc(1, sizeof(struct GroupCommunicationEstablishReq));
   group_comm_establish_req->type = type;
   group_comm_establish_req->sourceL2Id = source_id;
   group_comm_establish_req->groupL2Id = group_id;
   group_comm_establish_req->groupIpAddress = group_ip_address;
   group_comm_establish_req->pppp = pppp;

   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = GROUP_COMMUNICATION_ESTABLISH_REQ;
   memcpy(&sl_ctrl_msg->sidelinkPrimitive.group_comm_establish_req, group_comm_establish_req,sizeof(struct GroupCommunicationEstablishReq));
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the message to OAI
   int serverlen = sizeof(sin);
   int n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(sin);
   n = recvfrom(sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   printf("Received on socket from OAI, msg type: %d\n",sl_ctrl_msg->type);
   if (sl_ctrl_msg->type == GROUP_COMMUNICATION_ESTABLISH_RSP) {
      slrb_id = sl_ctrl_msg->sidelinkPrimitive.slrb_id;
      printf("Received GroupCommunicationEstablishResponse on socket from OAI, slrb %d\n",slrb_id);
      return slrb_id;
   } else {
      return -1;
   }
}

//--------------------------------------------------------
int release_PC5u_group_request(uint8_t slrb_id)
{
   char send_buf[BUFSIZE], receive_buf[BUFSIZE];
   int status;
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int n;

   printf("------------------------------------------------\n");
   printf("Send GroupCommunicationReleaseRequest message to OAI (msg type: %d )\n",GROUP_COMMUNICATION_RELEASE_REQ);
   sl_ctrl_msg = calloc(1, sizeof(struct sidelink_ctrl_element));
   sl_ctrl_msg->type = GROUP_COMMUNICATION_RELEASE_REQ;
   sl_ctrl_msg->sidelinkPrimitive.slrb_id = slrb_id;
   memcpy((void *)send_buf, (void *)sl_ctrl_msg, sizeof(struct sidelink_ctrl_element));

   // send the GroupCommunicationReleaseRequest message to the OAI
   int serverlen = sizeof(sin);
   n = sendto(sockfd, (char *)send_buf, sizeof(struct sidelink_ctrl_element), 0, (struct sockaddr *)&sin, serverlen);
   if (n < 0)
      error("ERROR: Failed to send to OAI");

   // get the reply from OAI
   memset(receive_buf, 0, BUFSIZE);
   serverlen = sizeof(sin);
   n = recvfrom(sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&sin, &serverlen);
   if (n < 0)
      error("ERROR: Failed to receive from OAI");

   sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
   if (sl_ctrl_msg->type == GROUP_COMMUNICATION_RELEASE_RSP) {
      printf("Received GroupCommunicationReleaseResponse on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
      status = sl_ctrl_msg->sidelinkPrimitive.group_comm_release_rsp;
      printf("GroupCommunicationReleaseResponse, Status: %d\n", status);
      return status;
   } else {
      return -1;
   }

   return 0;
}

//--------------------------------------------------------
void *rcv_UE_status_notification(void *sock)
{
   char receive_buf[BUFSIZE];
   struct sidelink_ctrl_element *sl_ctrl_msg = NULL;
   int serverlen, n, status;

   printf("------------------------------------------------\n");
   printf("Thread to receive UEStateInformation from OAI\n");
   printf("Listening to incoming message from OAI.... \n");


   while (1) {
      memset(receive_buf, 0, BUFSIZE);
      serverlen = sizeof(sin);
      n = recvfrom(sockfd, receive_buf, BUFSIZE, 0, (struct sockaddr *)&sin, &serverlen);
      if (n < 0)
         error("ERROR: Failed to receive from OAI");

      sl_ctrl_msg = (struct sidelink_ctrl_element *) receive_buf;
      if (sl_ctrl_msg->type == UE_STATUS_INFO) {
         printf("Received UEStatusInformation on socket from OAI (msg type: %d)\n",sl_ctrl_msg->type);
         status =  sl_ctrl_msg->sidelinkPrimitive.ue_state;
         if (status == UE_STATE_ON_NETWORK ) {
            printf("[UEStatusInformation] UE is on coverage\n");
         } else if (status == UE_STATE_OFF_NETWORK) {
            printf("[UEStatusInformation] UE is out of coverage\n");
         }
      }
   }

   return 0;
}


//--------------------------------------------------------
void *generate_traffic()
{
   //generate PC5-U packet
   //system("ping 224.0.0.3 -I oip0 -c 10");
   //system("ping -b 10.10.10.255 -I oip0 -c 10"); //for broadcast
   //system("ping 10.10.10.2 -I oip0 -m 1 -c 10"); //for unicast traffic
   // system("iperf -c 224.0.0.3 -u -b 0.1M --bind 10.10.10.1 -t 100"); //iperf - multicast
   return 0;
}

//--------------------------------------------------------
// error - wrapper for perror
void error(char *msg) {
   perror(msg);
   exit(1);
}
